# Setting

```bash
dotnet new webapi -o DokuroMicroservice -f net7.0
dotnet new gitignore
# Create Dockerfile
fsutil file createnew Dockerfile 0
fsutil file createnew .dockerignore 0
docker build -t dokuromicroservice .
docker build -t dokuromicroservice .

```

```bash
git push --set-upstream https://gitlab.com/DokuroGitHub/DokuroMicroservice.git master
```

# Run

```bash
dotnet run --launch-profile https

```
